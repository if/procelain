module Simple where

import Data.List  (lookup)
import Data.Maybe (fromJust)



data Val =
  VVar String
  | VProd Val Val
  | VBool Bool
  | VLam  String Type Val
  | VApp  Val Val
  | VIf   Val Val Val
  | VSpl  Val String String Val
  deriving (Eq, Show)

data Type =
  TBool
  | TProd Type Type
  | TAbs  Type Type
  deriving (Eq, Show)



class Pretty a where
  pretty :: a -> String

instance Pretty Type where
  pretty TBool = "bool"
  pretty (TProd t1 t2) =
    '(':pretty t1 ++ ", " ++ pretty t2 ++ ")"
  pretty (TAbs  t1 t2) =
    '(':pretty t1 ++ " -> " ++ pretty t2 ++ ")"



instance Pretty Val where
  pretty (VVar x) = x
  pretty (VProd e1 e2) =
    '(' : pretty e1 ++ (',':pretty e2) ++ ")"
  pretty (VBool True) = ":t"
  pretty (VBool False) = ":f"
  pretty (VLam x _ e) =
    "(λ" ++ x ++ ". " ++ pretty e ++ ")"
  pretty (VApp e1 e2) =
    '(' : pretty e1 ++ (' ':pretty e2) ++ ")"
  pretty (VIf e1 e2 e3) =
    "(if " ++ pretty e1 ++ (' ':pretty e2)
    ++ (' ':pretty e3) ++ ")"
  pretty (VSpl e1 x y e2) =
    "(split " ++ pretty e1 ++ (' ':x) ++ (',':y)
    ++ (' ':pretty e2) ++ ")"


prettyPrint :: (Pretty a) => a -> IO ()
prettyPrint = putStrLn . pretty

type Context a = [(String, a)]

printContext :: (Pretty a) => Context a -> IO ()
printContext = print . map (\(x, t) -> (x, pretty t))


-- | Lookup the type of a variable,
-- and also "take" it by removing it
-- from the typing context.  This
-- assumes that the variable in question
-- is defined in the context.
takeVar :: String -> Context a -> (a, Context a)
takeVar x c =
  let (Just t) = lookup x c
      c' = filter (\(y, _) -> y /= x) c
  in (t, c')


check :: Context Type -> Val -> (Type, Context Type)
check c (VBool _) = (TBool, c)
check c (VVar  x) = takeVar x c
check c (VProd e1 e2) =
  let (t1, c')  = check c e1
      (t2, c'') = check c' e2
  in (TProd t1 t2, c'')
check c (VIf e1 e2 e3) =
  let (TBool, c') = check c e1
      -- question: must c1' == c2' ?
      (t1', c1') = check c' e2
      (t2', c2') = check c' e3
  in if (t1' == t2') && (c1' == c2') then
       (t1', c1')
     else error "bad types in `if'!"
check c (VSpl e1 x y e2) =
  let (TProd t1a t1b, c') = check c e1
      d = [(x, t1a), (y, t1b)] ++ c'
      -- TODO: make sure that c'' doesn't contain x and y
      (t2, c'') = check d e2
  in (t2, c')
check c (VApp e1 e2) =
  let (TAbs t1a t1b, c') = check c e1
      (t2, c'') = check c' e2
  in if t1a == t2 then
       (t1b, c'')
     else error "bad types in apply!"
check c (VLam x t e) =
  let (e', c') = check ((x, t) : c) e
      -- TODO: make this less clumsy when you refine scopes.
      -- it should remove the parameter's binding, but
      -- leave any bindings that the parameter had shadowed
      (_, c'') = takeVar x c'
  in (TAbs t e', c'')
check _ _ = error "unimplemented!"



eval :: Context Val -> Val -> (Val, Context Val)
eval c (VProd v1 v2) =
  let (v1', c') = eval c v1
      (v2', c'') = eval c' v2
  in (VProd v1' v2', c'')
eval c (VIf v1 v2 v3) =
  let (VBool b, c') = eval c v1
  in if b then eval c' v2
     else eval c' v3
eval c (VSpl v1 x y v2) =
  let (VProd p1 p2, c') = eval c v1
      c'' = [(x, p1), (y, p2)] ++ c'
  in eval c'' v2
eval c (VApp v1 v2) =
  let (VLam x _ e, c') = eval c v1
      (v2', c'') = eval c' v2
      c''' = (x, v2') : c''
  in eval c''' e
eval c v = (v, c)





main :: IO ()
main = do
  let c = [("x", TBool), ("f", TAbs TBool (TProd TBool TBool))]
      v = VApp (VLam "y" TBool (VApp (VVar "f") (VVar "y"))) (VVar "x")
      (t, c') = check c v

  prettyPrint v
  printContext c
  putStrLn ":::: *machine sounds*"
  prettyPrint t
  printContext c'

